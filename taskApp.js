/* eslint-disable */
const d = document;
const taskInput = d.querySelector('#taskInput');
const addBtn = d.querySelector('#addBtn');
const taskList = d.querySelector('#taskList');
const completedTaskList = d.querySelector('#completedTaskList');
const completedTasks = completedTaskList.childNodes;
const editBtn = d.querySelector('.editBtn');
const completedHeading = d.querySelector('h1.completed');

function handleFirstTab(event) {
  if (event.keyCode === 9) {
    // the "I am a keyboard user" key
    d.body.classList.add('user-is-tabbing');
    window.removeEventListener('keydown', handleFirstTab);
  }
}

function createNewTask(text) {
  if (taskInput.value.length > 0) {
    const listItem = d.createElement('li');
    const innerTaskItem = `
    	<input type='checkbox' class='checkbox'>
    	<div class='taskText'>${text}</div>
    	<input type='text' class='editInput'>
    	<div class='buttonContainer'>
    		<button class='editBtn'></button>
    		<button class='deleteBtn'></button>
    	</div>
	`;

    listItem.innerHTML = innerTaskItem;
    return listItem;
  }
}

function addTask() {
  if (taskInput.value.length > 0) {
    const listItem = createNewTask(taskInput.value);
    taskList.appendChild(listItem);
    bindTaskEvents(listItem, completeTask);
    taskInput.value = '';
  } else {
    window.alert(`Don't forget to add a task!`);
  }
}

const deleteTask = function() {
  console.log('delete task');

  const listItem = this.parentNode.parentNode;
  const ul = listItem.parentNode;

  ul.removeChild(listItem);
  
  if ((completedTasks.length - 1) < 1) {
    completedHeading.classList.remove('visible');
    completedHeading.classList.add('hidden');
  }
};

const editTask = function() {
  console.log('edit task confirmed');
  const listItem = this.parentNode.parentNode;
  const editInput = listItem.querySelector('input.editInput');
  const label = listItem.querySelector('.taskText');
  const containsClass = listItem.classList.contains('editMode');
  const editBtnText = listItem.querySelector('.editBtn');

  if (containsClass) {
	label.innerText = editInput.value;
	editBtnText.innerText = '';
  } else {
	editInput.value = label.innerText;
	editBtnText.innerText = 'save';
  }

  listItem.classList.toggle('editMode');
};

const completeTask = function() {
  console.log('completed task');

  const listItem = this.parentNode;

  completedTaskList.appendChild(listItem);
  bindTaskEvents(listItem, incompleteTask);

  completedHeading.classList.add('visible');
};

const incompleteTask = function() {
  console.log('Incomplete Task...');

  const listItem = this.parentNode;
  taskList.appendChild(listItem);
  bindTaskEvents(listItem, completeTask);

  if ((completedTasks.length - 1) < 1) {
    completedHeading.classList.toggle('visible');
  }
};

addBtn.addEventListener('click', addTask);
taskInput.addEventListener('keyup', function(event) {
  if (event.keyCode === 13) {
    addTask();
  }
});



const bindTaskEvents = function(taskListItem, checkBoxEvent, event) {
  console.log('bind list item events');
  const deleteButton = taskListItem.querySelector('button.deleteBtn');
  deleteButton.onclick = deleteTask;

  const checkbox = taskListItem.querySelector('.checkbox');
  checkbox.onchange = checkBoxEvent;

  const editButton = taskListItem.querySelector('button.editBtn');
  editButton.onclick = editTask;
};

for (let i = 0; i < taskList.children.length; i++) {
  bindTaskEvents(taskList.children[i], completeTask);
}

for (let i = 0; i < completedTaskList.children.length; i++) {
  bindTaskEvents(completedTaskList.children[i], incompleteTask);
}
